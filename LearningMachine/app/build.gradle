apply plugin: 'com.android.application'

android {
    namespace "com.learningmachine.android.app"
    compileSdkVersion 34
    dexOptions {
        javaMaxHeapSize "4g"
    }
    defaultConfig {
        applicationId "com.learningmachine.android.app"
        minSdkVersion 31
        targetSdkVersion 34
        versionName "3.2.0"
        def buildNumber = System.getenv("BUILD_NUMBER")
        if (buildNumber != null) {
            versionCode buildNumber.toInteger()
            versionNameSuffix "-" + buildNumber
        } else {
            versionCode 62
        }

        testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    testOptions {
        unitTests.returnDefaultValues = true
    }

    signingConfigs {
        LearningMachineRelease {
            keyAlias System.getenv("SIGNING_KEY_ALIAS")
            keyPassword System.getenv("SIGNING_KEY_PASSWORD")
            storeFile file('../../keys/learning-machine.jks')
            storePassword System.getenv("SIGNING_STORE_PASSWORD")
        }
    }

    packagingOptions {
        exclude 'lib/x86_64/darwin/libscrypt.dylib'
        exclude "org/bitcoinj/crypto/mnemonic/wordlist/english.txt"
        exclude "org/bitcoinj/crypto/cacerts"
        exclude "org.bitcoin.production.checkpoints.txt"
        exclude "org.bitcoin.test.checkpoints.txt"
    }

    if (project.hasProperty("devBuild")) {
        // disable PNG crunching
        aaptOptions.cruncherEnabled = false

        // we're not doing splits,  but in case we ever did:
        splits.abi.enable = false
        splits.density.enable = false
    }

    flavorDimensions "env"

    productFlavors {
        dev {
            applicationId "com.learningmachine.android.app.dev"
            dimension "env"
            if (project.hasProperty("devBuild")) {
                // don't package all resources for dev builds
                resConfigs("en", "xxhdpi")
            }
        }
        staging {
            applicationId "com.learningmachine.android.app.staging"
            dimension "env"
        }
        production {
            dimension "env"
        }
    }

    buildTypes {
        release {
//            if (System.getenv("BUILD_NUMBER") == null) {
//                versionNameSuffix "-nontravis"
//            }

            applicationVariants.all { variant ->
                variant.outputs.all {
                    outputFileName = "${variant.name}-${versionName}.apk"
                }
            }

            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.LearningMachineRelease
        }
        debug {
            if (System.getenv("BUILD_NUMBER") == null) {
                versionNameSuffix "-debug"
            }
            buildConfigField "boolean", "DEBUG_BUILD", "true"
            debuggable true
        }
    }

    buildFeatures {
        dataBinding true
        viewBinding true
        buildConfig true
    }

    lintOptions {
        abortOnError false
    }
}

final SUPPORT_VERSION = '27.1.1'
final DAGGER_VERSION = '2.22.1'
final SLF4J_VERSION = '1.7.25'
final SPONGYCASTLE_VERSION = '1.56.0.0'
final PROTOBUF_VERSION = '3.3.1'
final TIMBER_VERSION = '4.5.1'
final JODATIMEANDROID_VERSION = '2.9.9'
final RETROFIT_VERSION = '2.5.0'
final LOGGINGINTERCEPTOR_VERSION = '3.4.1'
final GSON_VERSION = '2.8.6'
final RXJAVA_VERSION = '1.3.0'
final RXANDROID_VERSION = '1.2.1'
final RXLIFECYCLE_VERSION = '1.0'
final RXLINT_VERSION = '1.2'
final PICASSO_VERSION = '2.5.2'
final OKIO_VERSION = '2.2.2'

// android test
final ESPRESSO_VERSION = '3.0.1'

// test
final JUNIT_VERSION = '4.12'
final MOCKITO_VERSION = '5.15.2'
final ROBOLECTRIC_VERSION = '4.14.1'
final JODATIME_VERSION = '2.8.1'

// These must be forced until everyone gets on the same page
// ?: 18.0, Robolectric: 33.3.1-jre
final GUAVA_VERSION = '27.0.1-android'

configurations.all {
    resolutionStrategy {
        force "com.google.guava:guava:$GUAVA_VERSION"
    }
}

repositories {
    maven {
        url 'https://maven.google.com'
    }
}

dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation 'androidx.appcompat:appcompat:1.0.0'
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'com.google.android.material:material:1.0.0'
    implementation "org.slf4j:slf4j-log4j12:$SLF4J_VERSION"
    implementation "com.madgag.spongycastle:core:$SPONGYCASTLE_VERSION"
    implementation "com.google.protobuf:protobuf-java:$PROTOBUF_VERSION"
    implementation "com.google.dagger:dagger:$DAGGER_VERSION"
    annotationProcessor "com.google.dagger:dagger-compiler:$DAGGER_VERSION"
    implementation "com.jakewharton.timber:timber:$TIMBER_VERSION"
    implementation "net.danlew:android.joda:$JODATIMEANDROID_VERSION"
    implementation "com.squareup.retrofit2:retrofit:$RETROFIT_VERSION"
    implementation "com.squareup.retrofit2:converter-gson:$RETROFIT_VERSION"
    implementation "com.squareup.retrofit2:adapter-rxjava:$RETROFIT_VERSION"
    implementation "com.squareup.okhttp3:logging-interceptor:$LOGGINGINTERCEPTOR_VERSION"
    implementation "com.squareup.okio:okio:$OKIO_VERSION"
    implementation "com.google.code.gson:gson:$GSON_VERSION"
    implementation "io.reactivex:rxjava:$RXJAVA_VERSION"
    implementation "io.reactivex:rxandroid:$RXANDROID_VERSION"
    implementation "com.trello:rxlifecycle:$RXLIFECYCLE_VERSION"
    implementation "com.trello:rxlifecycle-android:$RXLIFECYCLE_VERSION"
    implementation "nl.littlerobots.rxlint:rxlint:$RXLINT_VERSION"
    implementation "com.squareup.picasso:picasso:$PICASSO_VERSION"
    implementation "com.google.guava:guava:$GUAVA_VERSION"

    androidTestImplementation('androidx.test.espresso:espresso-core:3.1.0', {
        exclude group: 'com.android.support', module: 'support-annotations'
    })
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.1.0'
//    androidTestImplementation "com.android.support.test:testing-support-lib:0.1"
    androidTestImplementation('androidx.test.espresso:espresso-contrib:3.1.0') {
        exclude group: 'com.android.support', module: 'appcompat'
        exclude group: 'com.android.support', module: 'support-v4'
        exclude module: 'recyclerview-v7'
    }
    androidTestImplementation "org.mockito:mockito-core:$MOCKITO_VERSION"
    androidTestImplementation('androidx.test.ext:junit:1.1.1')

    testImplementation "junit:junit:$JUNIT_VERSION"
    testImplementation "org.hamcrest:hamcrest:2.2"
    testImplementation "net.javacrumbs.json-unit:json-unit:2.33.0"
    testImplementation "org.json:json:20140107"
    testImplementation "org.mockito:mockito-core:$MOCKITO_VERSION"
    testImplementation "org.robolectric:robolectric:$ROBOLECTRIC_VERSION"
    testImplementation "joda-time:joda-time:$JODATIME_VERSION"
    testImplementation "androidx.test:core:1.6.1"
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
}
